package com.TeamGO.CSE110.test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

import com.TeamGO.CSE110.LoginSignupActivity;
import com.TeamGO.CSE110.R;
import com.parse.ParseUser;

public class LoginSignupActivityTest extends
		ActivityInstrumentationTestCase2<LoginSignupActivity> {
	
	//To be used to make main thread wait for UI thread
	private CountDownLatch lock = new CountDownLatch(1);
	
	private LoginSignupActivity currActivity;
	Button loginbutton;
	Button signup;
	EditText username;
	EditText password;
	
	public LoginSignupActivityTest(){
		super(LoginSignupActivity.class);
	}
	
	//Will be called before every method
	protected void setUp() throws Exception{
		super.setUp();
		currActivity = getActivity(); //Get the activity under test
		//Get each element in the UI
		username = (EditText) currActivity.findViewById(R.id.username);
		password = (EditText) currActivity.findViewById(R.id.password);
		loginbutton = (Button) currActivity.findViewById(R.id.login);
		signup = (Button) currActivity.findViewById(R.id.signup);
	}
	
	//Test case 1: Non-Existent user
	public void testANonexistentUser1() throws InterruptedException{
	    currActivity.runOnUiThread(new Runnable() {
	    	public void run() {
	    		username.setText("test_NonExistent");
	    		password.setText("12345678");
	    	    loginbutton.callOnClick();
	    	}
	    });
	    lock.await(5000, TimeUnit.MILLISECONDS); 
	    	//Wait 5 secs to allow Parse server return verification result.
	    assertTrue(!LoginSignupActivity.isLoggedIn());
	    	//Assert if login failed
	}
	
	//Test case 2: Non-Existent user
	public void testANonexistentUser2() throws InterruptedException{
	    currActivity.runOnUiThread(new Runnable() {
	    	public void run() {
	    		username.setText("test_NoExistent2");
	    		password.setText("dfdger");
	    	    loginbutton.callOnClick();
	    	}
	    });
	    lock.await(5000, TimeUnit.MILLISECONDS); 
    		//Wait 5 secs to allow Parse server return verification result.
	    assertTrue(!LoginSignupActivity.isLoggedIn());
    		//Assert if login failed
	}
	
	//Test case 3: Existent user with wrong password
	public void testBWrongPwd1() throws InterruptedException{
	    currActivity.runOnUiThread(new Runnable() {
	    	public void run() {
	    		username.setText("flyfire2002");
	    		password.setText("dfdger");
	    	    loginbutton.callOnClick();
	    	}
	    });
	    lock.await(5000, TimeUnit.MILLISECONDS); 
			//Wait 5 secs to allow Parse server return verification result.
	    assertTrue(!LoginSignupActivity.isLoggedIn());
			//Assert if login failed
	}
	
	//Test case 4: Existent user with wrong password
	public void testBWrongPwd2() throws InterruptedException{
	    currActivity.runOnUiThread(new Runnable() {
	    	public void run() {
	    		username.setText("SUOAFT1");
	    		password.setText("1234567");
	    	    loginbutton.callOnClick();
	    	}
	    });
	    lock.await(5000, TimeUnit.MILLISECONDS); 
			//Wait 5 secs to allow Parse server return verification result.
	    assertTrue(!LoginSignupActivity.isLoggedIn());
			//Assert if login failed
	}
	
	//Test case 5: Wrong credential pair
	public void testCWrongCredentialPair1() throws InterruptedException{
	    currActivity.runOnUiThread(new Runnable() {
	    	public void run() {
	    		username.setText("hello");
	    		password.setText("12345678");
	    	    loginbutton.callOnClick();
	    	}
	    });
	    lock.await(5000, TimeUnit.MILLISECONDS); 
			//Wait 5 secs to allow Parse server return verification result.
	    assertTrue(!LoginSignupActivity.isLoggedIn());
			//Assert if login failed
	}
	
	//Test case 5: Correct credential pair
	public void testDExistentUser1() throws InterruptedException{
	    currActivity.runOnUiThread(new Runnable() {
	    	public void run() {
	    		username.setText("flyfire2002");
	    		password.setText("12345678");
	    		loginbutton.callOnClick();
	    	}
	    });
	    lock.await(5000, TimeUnit.MILLISECONDS);
	    	//Wait 5 secs to allow Parse server return verification result.
	    assertTrue(LoginSignupActivity.isLoggedIn());
	    	//Assert if login succeeded
	    if (LoginSignupActivity.isLoggedIn())
			ParseUser.logOut();
			//Log out the user.
	}
}
	

