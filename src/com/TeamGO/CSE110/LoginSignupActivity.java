package com.TeamGO.CSE110;
 
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
 
import com.TeamGO.CSE110.R;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
 
public class LoginSignupActivity extends Activity {
	// Declare Variables
	static boolean loginSuccess; 
	Button loginbutton;
	Button signup;
	String usernametxt;
	String passwordtxt;
	EditText password;
	EditText username;
 
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		// Get the view from xml layout file
		setContentView(R.layout.loginsignup);
		
		// Locate EditTexts in xml layout file
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
 
		// Locate Buttons in xml layout
		loginbutton = (Button) findViewById(R.id.login);
		signup = (Button) findViewById(R.id.signup);
		
		loginSuccess = false;
 
		// Login Button Click Listener
		loginbutton.setOnClickListener(new OnClickListener() { 
 
			public void onClick(View arg0) {
				// Retrieve the text entered from the EditText
				usernametxt = username.getText().toString();
				passwordtxt = password.getText().toString();
 
				// Send data to Parse.com for verification
				ParseUser.logInInBackground(usernametxt, passwordtxt,
						new LogInCallback() {
							public void done(ParseUser user, ParseException e) {
								if (user != null) {
									
									loginSuccess = true;
									// If user exist and authenticated, send user to Welcome.class
									Intent intent = new Intent(
											LoginSignupActivity.this,
											Welcome.class);
									startActivity(intent);
									Toast.makeText(getApplicationContext(),
											"You are logged in.",
											Toast.LENGTH_LONG).show();

									//finish(); //Doesn't terminate the activity when logged in
									            //so we can get back here if necessary.
								} else {
									loginSuccess = false;
									Toast.makeText(
											getApplicationContext(),
											"No such user exists. Please signup.",
											Toast.LENGTH_LONG).show();
								}
							}
						});
			}
		});
		
		// Sign up Button Click Listener
		signup.setOnClickListener(new OnClickListener() {
 
			public void onClick(View arg0) {
				//on click, display screen to choose account type for registration
				Intent intent = new Intent(
						LoginSignupActivity.this,
						SignUpOptionActivity.class);
				startActivity(intent);
				
 
			}
		});
 
	}
	
	public static boolean isLoggedIn(){
		return loginSuccess;
	}
	
	/*
	 * This function is called when the login page is again visible for whatever reason
	 * (return from SignUpOptionActivity, logging out, etc.)
	 */
	public void onStart(){
		super.onStart();
		//Clear the username and password field if this is a restart after logout
		if (loginSuccess){
			username.setText("");
			password.setText("");
		}
		loginSuccess = false;
	}
}