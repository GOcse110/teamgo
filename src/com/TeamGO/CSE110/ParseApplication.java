package com.TeamGO.CSE110;
 
import com.parse.Parse;
import com.parse.ParseACL;
 
import com.parse.ParseUser;
 
import android.app.Application;
 
public class ParseApplication extends Application {
 
    @Override
    public void onCreate() {
        super.onCreate();
 
        // Parse key codes
        Parse.initialize(this, "AMTvK2ZetdSyAHBIiVnZaf4UVdOcLAfCTGsZdnke", "yFBpW6GF61xbO8N9PT3qTbIDLuESuyzSXMGx2LHP");
 
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
 
        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);
 
        ParseACL.setDefaultACL(defaultACL, true);
    }
 
}