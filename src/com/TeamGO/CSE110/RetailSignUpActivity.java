package com.TeamGO.CSE110;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RetailSignUpActivity extends Activity {

	// Declare Variables
	Button signup;
	
	//Textbox Labels
	String nametxt;
	String addresstxt;
	String usernametxt;
	String passwordtxt;
	
	//Empty Text Entry Forms
	EditText name;
	EditText address;
	EditText password;
	EditText username;
 
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Get the view from layout xml
		setContentView(R.layout.activity_retail_sign_up);
		
		// Locate EditTexts in layout xml
		name = (EditText) findViewById(R.id.name);
		address = (EditText) findViewById(R.id.address);
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
 
		// Locate Buttons in xml
		signup = (Button) findViewById(R.id.signup);
 
 

		// Sign up Button Click Listener
		signup.setOnClickListener(new OnClickListener() {
 
			public void onClick(View arg0) {
				
				// Retrieve the text entered from the EditText
				usernametxt = username.getText().toString();
				passwordtxt = password.getText().toString();
				nametxt = name.getText().toString();
				addresstxt = address.getText().toString();
 
				// Force user to fill out all fields of form
				if (usernametxt.equals("") || passwordtxt.equals("")
						||nametxt.equals("") || addresstxt.equals("")) {
				
						Toast.makeText(getApplicationContext(),
								"Please complete the sign up form",
								Toast.LENGTH_LONG).show();
				} else {
					// Save new user data into Parse.com Data Storage
					ParseUser user = new ParseUser();
					user.setUsername(usernametxt);
					user.setPassword(passwordtxt);
					user.put("name", nametxt);
					user.put("address", addresstxt);
					
					user.signUpInBackground(new SignUpCallback() {
						public void done(ParseException e) {
							if (e == null) {
								//pull up login screen
								Intent intent = new Intent(
										RetailSignUpActivity.this,
										LoginSignupActivity.class);
								startActivity(intent);
								// Show a simple Toast message upon successful registration
								Toast.makeText(getApplicationContext(),
										"Successfully signed up. Please log in.",
										Toast.LENGTH_LONG).show();
								//Upon successful login, kill the SignUpOptionActivity.
								SignUpOptionActivity.SUOA.finish();
								finish();
							} 
							//handle duplicate username case
							else if(e.getCode() == 202){
								Toast.makeText(getApplicationContext(),
										"Username is already taken. Choose a different username.",
								Toast.LENGTH_LONG).show();
							}
							else {
								Toast.makeText(getApplicationContext(),
										"Sign up Error"+ e.getCode(), Toast.LENGTH_LONG)
										.show();
							}
						}
					});
				}
				

			}
		});
 
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
